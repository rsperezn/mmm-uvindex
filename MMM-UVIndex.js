/* global Module */

/* Magic Mirror
 * Module: UVIndex
 *
 * By Santiago Perez https://bitbucket.org/rsperezn
 * MIT Licensed.
 */

Module.register("MMM-UVIndex", {

    // Default module config.
    defaults: {
        location: false,
        locationID: false,
        accessToken: "",
        updateInterval: 30 * 60 * 1000, // every 30 minutes
        animationSpeed: 1000,
        lang: config.language,
        initialLoadDelay: 0, // 0 seconds delay
        retryDelay: 2500,
        appendLocationNameToHeader: true,
        apiVersion: "v1",
        apiBase: "https://api.openuv.io/api/",
        UVIndexEndpoint: "forecast",
        maxNumberOfIndicesToShow: 5,
        customHeader: undefined,
        latitude: -33.86, //Sydney info
        longitude: 151.21
    },

    // create a variable for the first upcoming calendar event. Used if no location is specified.
    firstEvent: false,

    // create a variable to hold the location name based on the API result.
    fetchedLocationName: "",

    // Define required scripts.
    getScripts: function () {
        return ["moment.js"];
    },

    // Define required scripts.
    getStyles: function () {
        return ["uvindex.css"];
    },

    // Define required translations.
    getTranslations: function () {
        // The translations for the default modules are defined in the core translation files.
        // Therefor we can just return false. Otherwise we should have returned a dictionary.
        // If you're trying to build your own module including translations, check out the documentation.
        return false;
    },

    // Define start sequence.
    start: function () {
        Log.info("Starting module: " + this.name);

        // Set locale.
        moment.locale(config.language);

        this.weatherType = null;
        this.loaded = false;
        this.scheduleUpdate(this.config.initialLoadDelay);

    },

    createCurrentTable: function () {
        function createRowWithText(text, elementType, color = undefined) {
            const rowElement = document.createElement("tr");
            const tableCellElement = document.createElement(elementType);
            tableCellElement.innerHTML = text;
            tableCellElement.setAttribute("style", "text-align:center;color:" + color);
            rowElement.appendChild(tableCellElement);
            return rowElement;
        }


        const currentUVIndexTable = document.createElement("table");
        currentUVIndexTable.className = "bright medium currentUVIndexTable";

        const currentUVIndexTimeRow = createRowWithText("Now", "th");
        currentUVIndexTable.appendChild(currentUVIndexTimeRow);

        let text;
        let color;
        if (this.currentUVIndex) {
            let uvIndexInfo = this.getUVIndexLevel(this.currentUVIndex.uv);
            text = uvIndexInfo.fullName;
            color = uvIndexInfo.color
        } else {
            text = "No UV readings";
            color = "";
        }

        const currentUVIndexLevelRow = createRowWithText(text, "td", color);
        currentUVIndexTable.appendChild(currentUVIndexLevelRow);

        const currentUVIndexMeasurementRow = createRowWithText(this.currentUVIndex ? this.currentUVIndex.uv : 0, "td");
        currentUVIndexTable.appendChild(currentUVIndexMeasurementRow);

        return currentUVIndexTable;

    },

    createForecastTable: function () {
        function appendCellsToRow(cellData, row, elementType, levelColors = undefined) {
            cellData.forEach((data, index) => {
                    const htmlTableHeaderCellElement = document.createElement(elementType);
                    htmlTableHeaderCellElement.innerText = data;
                    if (levelColors) {
                        htmlTableHeaderCellElement.setAttribute("style", "color:" + levelColors[index]);
                    }
                    row.appendChild(htmlTableHeaderCellElement);
                }
            );
        }

        const forecastUVIndexTable = document.createElement("table");
        forecastUVIndexTable.className = "dimmed light small";

        const forecastTimeRow = document.createElement("tr");
        const formattedHours = [];
        for (let i = 1; i <= this.config.maxNumberOfIndicesToShow; i++) {
            const forecastDate = new Date();
            forecastDate.setHours(forecastDate.getHours() + i);
            formattedHours.push(forecastDate.getHours() + ":00");
        }

        appendCellsToRow(formattedHours, forecastTimeRow, "th");
        forecastUVIndexTable.appendChild(forecastTimeRow);


        const forecastIndexLevelRow = document.createElement("tr");
        const formattedLevel = [];
        const levelColors = [];

        this.forecastUVIndices.forEach(it => {
            if (it) {
                let levelInfo = this.getUVIndexLevel(it.uv);
                formattedLevel.push(levelInfo.shortName);
                levelColors.push(levelInfo.color);
            } else {
                return "N/A"
            }
        });

        appendCellsToRow(formattedLevel, forecastIndexLevelRow, "td", levelColors);
        forecastUVIndexTable.appendChild(forecastIndexLevelRow);

        const forecastMeasurementRow = document.createElement("tr");
        const formattedMeasurements = this.forecastUVIndices.map(it => it ? it.uv : 0);

        appendCellsToRow(formattedMeasurements, forecastMeasurementRow, "td");
        forecastUVIndexTable.appendChild(forecastMeasurementRow);

        return forecastUVIndexTable;

    },

    getUVIndexLevel: function (uv) {
        if (uv >= 0 && uv < 3) {
            return {"fullName": "Low", "shortName": "Low", "color": "#558B2F"};
        } else if (uv >= 3 && uv < 6) {
            return {"fullName": "Moderate", "shortName": "Mod", "color": "#F9A825"};
        } else if (uv >= 6 && uv < 8) {
            return {"fullName": "High", "shortName": "High", "color": "#EF6C00"};
        } else if (uv >= 8 && uv < 11) {
            return {"fullName": "Very High", "shortName": "VHigh", "color": "#B71C1C"};
        } else if (uv >= 11) {
            return {"fullName": "Extreme", "shortName": "Extr", "color": "#6A1B9A"};
        } else {
            return {"fullName": "Invalid UV Index measurement", "shortName": "Invalid", "color": "#a3a9b3"};
        }
    },

    // Override dom generator.
    getDom: function () {
        var wrapper = document.createElement("div");

        if (this.config.appid === "") {
            wrapper.innerHTML = "Please set the correct openUV.io <i>accessToken</i> in the config for module: " + this.name + ".";
            wrapper.className = "dimmed light small";
            return wrapper;
        }

        if (!this.loaded) {
            wrapper.innerHTML = this.translate("LOADING");
            wrapper.className = "dimmed light small";
            return wrapper;
        }

        const currentUVIndexTable = this.createCurrentTable();
        wrapper.appendChild(currentUVIndexTable);


        const forecastUVIndexTable = this.createForecastTable();
        wrapper.appendChild(forecastUVIndexTable);

        return wrapper;
    },

    // Override getHeader method.
    getHeader: function () {
        return this.config.customHeader ? this.config.customHeader : "UV Index"
    },

    // Override notification handler.
    notificationReceived: function (notification, payload, sender) {
        if (notification === "DOM_OBJECTS_CREATED") {
            if (this.config.appendLocationNameToHeader) {
                this.hide(0, {lockString: this.identifier});
            }
        }
    },

    /* updateUVIndex
     * Requests new data from https://www.openuv.io/forecast
     * Calls processUVIndex on successful response.
     */
    updateUVIndex: function () {
        if (this.config.appid === "") {
            Log.error("UVIndex: APPID not set!");
            return;
        }

        const url = this.config.apiBase + this.config.apiVersion + "/" + this.config.UVIndexEndpoint + this.getParams();
        const self = this;
        let retry = true;

        const UVIndexRequest = new XMLHttpRequest();
        UVIndexRequest.open("GET", url, true);
        UVIndexRequest.setRequestHeader("x-access-token", self.config.accessToken);
        UVIndexRequest.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    self.processUVIndex(JSON.parse(this.response));
                } else if (this.status === 401) {
                    self.updateDom(self.config.animationSpeed);

                    Log.error(self.name + ": Incorrect APPID.");
                    retry = true;
                } else {
                    Log.error(self.name + ": Could not load weather.");
                }

                if (retry) {
                    self.scheduleUpdate((self.loaded) ? -1 : self.config.retryDelay);
                }
            }
        };
        UVIndexRequest.send();
    },

    /* getParams
     * Generates an url with api parameters based on the config.
     *
     * return String - URL params.
     */
    getParams: function () {
        let params = "?";
        params += "lat=" + this.config.latitude;
        params += "&lng=" + this.config.longitude;
        return params;
    },


    /* processUVIndex(data)
     * Uses the received data to set the various values.
     *
     * argument data object - Weather information received form https://www.openuv.io/
     */
    processUVIndex: function (data) {
        if (!data || !data.result) {
            LOG.warn("Did not receive any data for request");
            return;
        }

        //only keep YYYY-MM-ddTHH other parameters are not important
        const currentSimpleDateTime = new Date().toISOString().slice(0, 13);

        const mappedResults = data.result.map(it => {
            const mappedObject = {};
            mappedObject.uv = Math.round(it.uv * 100) / 100; //truncate to 2 decimals
            mappedObject.simpleDateTime = it.uv_time.slice(0, 13);
            return mappedObject;
        });

        const relevantEvents = mappedResults
            .filter(it => it.simpleDateTime >= currentSimpleDateTime);

        this.currentUVIndex = undefined;
        this.forecastUVIndices = [];
        const numberOfUVIndices = relevantEvents.length;
        if (numberOfUVIndices === 0) {
            this.currentUVIndex= undefined;
            for (let i = 0; i < this.config.maxNumberOfIndicesToShow; i++) {
                this.forecastUVIndices.push(undefined);
            }
        } else if (numberOfUVIndices > 0 && numberOfUVIndices < this.config.maxNumberOfIndicesToShow) {
            this.currentUVIndex = relevantEvents[0];
            this.forecastUVIndices = relevantEvents.slice(1);
            const indicesToFill = (this.config.maxNumberOfIndicesToShow - this.forecastUVIndices.length);
            for (let i = 0; i < indicesToFill; i++) {
                this.forecastUVIndices.push(undefined);
            }
        } else {
            this.currentUVIndex = relevantEvents[0];
            this.forecastUVIndices = relevantEvents.slice(1, 6);
        }

        this.show(this.config.animationSpeed, {lockString: this.identifier});
        this.loaded = true;
        this.updateDom(this.config.animationSpeed);
        this.sendNotification("UVINDEX_DATA", {data: data});
    },

    /* scheduleUpdate()
     * Schedule next update.
     *
     * argument delay number - Milliseconds before next update. If empty, this.config.updateInterval is used.
     */
    scheduleUpdate: function (delay) {
        let nextLoad = this.config.updateInterval;
        if (typeof delay !== "undefined" && delay >= 0) {
            nextLoad = delay;
        }

        const self = this;
        setTimeout(() => {
            self.updateUVIndex();
        }, nextLoad);
    },
});
