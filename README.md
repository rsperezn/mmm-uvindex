# Module: UV Index
The `UVIndex` module displays the current UV Index for a given location specified by its latitude and longitude
## Screenshot

- UV Index screenshot
![UV Index Screenshot](sampleUVIndex_screenshot.png)

## Using the module

To use this module first clone it from `git clone https://rsperezn@bitbucket.org/rsperezn/MMM-UVIndex.git` into your `/MagicMirror/modules/` directory. Then add it to the modules array in the `config/config.js` file:
````javascript
modules: [
	{
		module: "MMM-UVIndex",
		position: "top_right",	// This can be any of the regions.
									// Best results in left or right regions.
		config: {
			latitude: -33.86 , //simply Google these values for the location you are interested in knowing the UV Index
			longitude: 151.20, 
			accessToken: "abcde12345abcde12345abcde12345ab", //You can get one for free at https://www.openuv.io
            customHeader: "Sydney's UV Index" //Defaults to UV Index if not provided
		}
	}
]
````

This module will display the current UV index and the value for the next 5 hours. These values will automatically updated every 30 minutes